# Basic tasks for loops

## Legend
has it that **N** is a mythical integer introduced to us by the user... 
**C** is a similar mythical symbol...

## Tasks
1. Write a program to display first 10 natural number
1. Write a program to display natural number in [1, **N**)
1. Write a program to display natural number in [1, **N**]
1. Write a program to find the sum of first 10 natural number
1. Write a program to find the sum of natural number in [1, **N**)
1. Write a program to find the sum of natural number in [1, **N**]
1. Write a program to display 
