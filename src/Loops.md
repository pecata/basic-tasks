#define WRP Write a program to
#define NN natural number
# Basic tasks for loops

#include Legend.hlp

## Tasks
1. WRP display first 10 NN
1. WRP display NN in [1, **N**)
1. WRP display NN in [1, **N**]
1. WRP find the sum of first 10 NN
1. WRP find the sum of NN in [1, **N**)
1. WRP find the sum of NN in [1, **N**]
1. WRP display 
